package com.branch_poc;

import android.content.Intent;
import com.facebook.react.ReactActivity;
import io.branch.rnbranch.RNBranchModule;
import io.branch.rnbranch.RNBranchPackage;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "branch_poc";
    }

    @Override
    protected void onStart() {
        super.onStart();
        RNBranchModule.initSession(this.getIntent().getData(), this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        RNBranchModule.onStop();
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }
}
